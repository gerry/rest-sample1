import Route from '@ember/routing/route';
import RouteMixin from 'ember-cli-pagination/remote/route-mixin';

export default Route.extend(RouteMixin, {
  //optional. default is 10
  perPage: 10,

  model(params){
    // return this.get('store').findAll('post');
    params.paramMapping = {
      page: "_page",
      perPage: "_limit"};
    return this.findPaged('post', params);
  }
});
