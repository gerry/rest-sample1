import Route from '@ember/routing/route';
import { Promise } from "rsvp";
export default Route.extend({
  model(){
    // load all data

    // const users = this.store.findAll('user');
    // const albums = this.store.findAll('album');
    // const photos = this.store.findAll('photo');
    // const comments = this.store.findAll('comment');
    // const todos = this.store.findAll('todo');
    // const posts = this.store.findAll('post');
    //
    // return Promise.all([
    //   users,
    //   albums,
    //   photos,
    //   comments,
    //   todos,
    //   posts
    // ]);

    // find all posts with user id
    // return this.store.query('post', {
    //   userId: 1
    // });

    //paginate
    //https://jsonplaceholder.typicode.com/posts?_page=1&_limit=20

    // find all posts with user id & include comments
    this.store.query('post', {
      userId: 1
    }).then((values) => {
      return this.store.query('comment', {
        postId: values.get('firstObject.id')
      })
    });
  }
});
