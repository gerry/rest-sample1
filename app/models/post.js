import DS from 'ember-data';
const { Model, attr, belongsTo, hasMany } = DS;

export default Model.extend({
  title: attr('string'),
  body: attr('string'),
  user: belongsTo('user'),
  comments: hasMany('comment')
});
