import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('posts');
  this.route('comments');
  this.route('albums');
  this.route('users');
  this.route('photos');
  this.route('todos');
  this.route('all');
});

export default Router;
